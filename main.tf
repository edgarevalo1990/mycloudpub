resource "aws_instance" "Practicas-Globant" {
  ami               = aws_ami.nombreDeTuAmi.id #hace referencia al nombre establecido en createAmi.tf
  instance_type     = "t2.micro"
  availability_zone = "sa-east-1c" #elegi South America por cuestiones de latencia

  ebs_block_device {
    device_name = "/dev/xvda"
    volume_size = 30


  }
  tags = {
    Name = "Practicas-Globant"
  }
  key_name        = "nombreDeTuKey" #nombre de la key a usar para acceder a la instancia
  security_groups = ["ssh-allow"]   #nombre del security group a usar

}
