resource "aws_ami" "nombreDeTuAmi" {
  name                = "nombreAMostrar"
  virtualization_type = "hvm"
  root_device_name    = "/dev/xvda"

  ebs_block_device { #establece el procedimiento de montado de discos en la instancia nueva
    device_name = "/dev/xvda"
    snapshot_id = aws_ebs_snapshot.nombreDeSnapshotDeReferencia.id
    volume_size = 20
  }
}
